import { EventEmitter } from "events";
export const LeaderboardEvent = new EventEmitter();

export function newLeaderBoard() {
    LeaderboardEvent.emit('NewLeaderBoard');
};


