import mongoose, { Schema, Types } from "mongoose";
import bcrypt from "bcrypt";
import { GAME_MODEL, USER_MODEL } from "../../app.model";

const UserSchema = Schema({
    name: { type: String },
    email: { type: String, lowercase: true, unique: true },
    password: { type: String },
    salt: { type: String },
    lastGame: { type: mongoose.Types.ObjectId, ref: GAME_MODEL },
    gamesWon: { type: Number, default: 0 },
    gamesLost: { type: Number, default: 0 },
    totalPoints: { type: Number, default: 0 }
}, {
    timestamps: true
});

UserSchema.pre("save", async function(next) {

    if(!this.isModified("password")) return next();

    this.salt = await bcrypt.genSalt(10);
    this.password = await bcrypt.hash(this.password, this.salt);

    next();
});

UserSchema.methods.comparePassword = async function(inputPassword, originalPassword) {
    return await bcrypt.compare(inputPassword, originalPassword);
};

export default mongoose.model(USER_MODEL, UserSchema);