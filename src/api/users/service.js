import User from './model';
import { generateToken } from "../../utils/token.service";
import { AppError } from '../../utils/error.service';
import { updateLeaderboard } from '../../utils/redis.service';
import { newLeaderBoard } from './events';

export async function verifyUser({email, password}) {

    const user = await User.findOne({ email: email.toLowerCase()}).populate('lastGame');
    if (!user || !(await user.comparePassword(password, user.password))) {
        throw AppError(400, 'Verification Error', 'Email or Password is wrong');
    };

    const token = generateToken(user._id);

    return { token, user: {name: user.name, email: user.email, gamesWon: user.gamesWon, gamesLost: user.gamesLost, totalPoints: user.totalPoints, lastGame: user.lastGame || null} };
};

export async function createUser({name, email, password}) {

    const user = await User.create({name, email, password});
    if (!user) throw AppError(null, null, 'Something Went Wrong');
    
    const token = generateToken(user._id);

    return { token, user: {name: user.name, email: user.email, gamesWon: user.gamesWon, gamesLost: user.gamesLost, totalPoints: user.totalPoints, lastGame: null} };
};

export async function getUser(id) {

    const user = await User.findById(id, 'name email gamesWon gamesLost totalPoints lastGame').populate('lastGame');
    if (!user) throw AppError(400, 'Not Found', 'User Not Exist');

    return user;
};

export async function getUserByEmail(email) {
    const user = await User.findOne({ email }).lean();
    return user;
};

export async function updateLastGame(userId, gameId) {

    const user = await User.findByIdAndUpdate(userId, { lastGame: gameId }, { new: true });
    if (!user) throw AppError(null, 'Update Error', 'Something Went Wrong');

};

export async function updateGameWon(userId) {
    let user = await getUser(userId);

    user.gamesWon += 1;
    user.totalPoints += 1;

    const saved = await user.save();
    if (!saved) throw AppError(null, 'Update Error', 'Something Went Wrong');

    updateLeaderboard();
    newLeaderBoard();
};

export async function updateGameLost(userId) {
    let user = await getUser(userId);

    user.gamesLost += 1;

    const saved = await user.save();
    if (!saved) throw AppError(null, 'Update Error', 'Something Went Wrong');

};

export async function getLeaderBoardList() {
    const list = await User.find({}, 'name totalPoints').lean();
    if (!list.length) return [];
    return list;
};