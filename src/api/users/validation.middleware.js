import { body } from "express-validator";
import { getUserByEmail } from "./service";

export function signupValidation() {
    return [
        body('name', 'Username Required').exists(),
        body('email', 'Email is Required').exists().isEmail().withMessage('Input email is Wrong').custom(async email => {
            const user = await getUserByEmail(email);
            if (user) return Promise.reject('Email Already In Use');
            return true;
        }),
        body('password', 'Password Required').exists()
    ];
};

export function loginValidation() {
    return [
        body('email', 'Email is Required').exists().isEmail().withMessage('Input email is Wrong'),
        body('password', 'Password Required').exists()
    ];
};