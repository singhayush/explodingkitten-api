import { Router } from "express";
import { validationErrHandler } from "../../Middleware/errorHandler";
import { login, signup } from "./controller";
import { loginValidation, signupValidation } from "./validation.middleware";

const router = new Router();


router.post('/signup', signupValidation(), validationErrHandler, signup);
router.post('/login', loginValidation(), validationErrHandler, login);

module.exports = router;