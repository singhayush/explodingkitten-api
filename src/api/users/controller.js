import { createUser, verifyUser } from "./service";

export async function login(req, res, next) {
    try {
        const {email, password} = req.body;
        
        const data = await verifyUser({ email, password });

        return res.status(200).send(data);

    } catch (error) {
        next(error);
    }
};

export async function signup(req, res, next) {
    try {
        const data = await createUser(req.body);

        return res.status(201).send(data);
    } catch (error) {
        next(error);
    }
};