import { Router } from "express";
import { protect } from "../../Middleware/auth";
import { validationErrHandler } from "../../Middleware/errorHandler";
import { getExistingGame, newGame, updateExistingGame } from "./controller";
import { newGameValidation, updateGameValidation } from "./validation.middleware";

const router = new Router();

router.use(protect);

router.post('/newGame', newGameValidation(), validationErrHandler, newGame);

router
    .route('/:gameId')
    .get(getExistingGame)
    .put(updateGameValidation(), validationErrHandler, updateExistingGame);


module.exports = router;