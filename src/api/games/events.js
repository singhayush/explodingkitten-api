import { updateGame } from "./service";

function onGameUpdateSocketListener(data) {
    const {result, moves, gameId} = data;
    updateGame({result, moves, gameId});
};

export function registerGameSocket(socket) {
    socket.on('Update-Game', onGameUpdateSocketListener);
};