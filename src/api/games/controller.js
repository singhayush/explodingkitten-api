import { saveNewGame, getGame, updateGame } from "./service";

export async function newGame(req, res, next) {
    try {
        const user = req.user._id;
        const moves = req.body.moves;

        await saveNewGame({ user, moves });

        return res.status(201).send('Successfully Created');
    } catch (error) {
        next(error);
    }
};

export async function getExistingGame(req, res, next) {
    try {
        const game = await getGame(req.params.gameId);

        return res.status(200).send(game);
    } catch (error) {
        next(error);
    }
};

export async function updateExistingGame(req, res, next) {
    try {
        const {result, moves} = req.body;
        const {gameId} = req.params;

        await updateGame({ result, moves, gameId });

        return res.status(200).send('Successfully Updated');
    } catch (error) {
        next(error);
    }
};