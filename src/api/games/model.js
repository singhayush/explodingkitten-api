import mongoose, { Schema, Types } from 'mongoose';
import { CARD_ACTIONS, CARD_TYPES, GAME_MODEL, GAME_RESULT, USER_MODEL } from '../../app.model';

const GameSchema = new Schema({
    result: { type: String, enum: Object.values(GAME_RESULT) },
    moves: [{
        cardDrawn: { type: String, enum: Object.values(CARD_TYPES) },
        action: { type: String, enum: Object.values(CARD_ACTIONS) },
        deck: [{ type: String, enum: Object.values(CARD_TYPES) }]
    }],
    user: { type: mongoose.Types.ObjectId, ref: USER_MODEL }
}, { timestamps: true });

export default mongoose.model(GAME_MODEL, GameSchema);