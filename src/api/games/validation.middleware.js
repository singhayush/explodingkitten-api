import { body, check } from "express-validator";
import { CARD_ACTIONS, CARD_TYPES, GAME_RESULT } from "../../app.model";

export function newGameValidation() {
    return [
        check('moves').optional().custom(array => {
            if (!Array.isArray(array)) return Promise.reject('Only Array is accepted');
            if (array.length) {
                for (let move of array) {
                    if(!move.cardDrawn || !Object.values(CARD_TYPES).includes(move.cardDrawn)) return Promise.reject('Invalid Card');
                    if(!move.action || !Object.values(CARD_ACTIONS).includes(move.action)) return Promise.reject('Invalid Action');
                    if(!move.deck || !move.deck.every(card => Object.values(CARD_TYPES).includes(card))) return Promise.reject('Invalid Deck Collection');
                };
            };
            return true;
        })
    ];
};

export function updateGameValidation() {
    return [
        body('result').exists().custom(value => {
            if(!Object.values(GAME_RESULT).includes(value)) return Promise.reject('Invalid Result Value');
            return true;
        }),
        check('moves').exists().custom(array => {
            if (!array || !Array.isArray(array)) return Promise.reject('Only Array is accepted');
            if (!array.length) return Promise.reject('Moves are Required');
            for (let move of array) {
                if(!move.cardDrawn || !Object.values(CARD_TYPES).includes(move.cardDrawn)) return Promise.reject('Invalid Card');
                if(!move.action || !Object.values(CARD_ACTIONS).includes(move.action)) return Promise.reject('Invalid Action');
                if(!move.deck || !move.deck.every(card => Object.values(CARD_TYPES).includes(card))) return Promise.reject('Invalid Deck Collection');
            };
            return true;
        })
    ];
};