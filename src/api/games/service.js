import { GAME_RESULT } from "../../app.model";
import Game from './model';
import { updateLastGame, updateGameWon } from "../users/service";
import { AppError } from "../../utils/error.service";


export async function saveNewGame({ user, moves }) {
    const result = GAME_RESULT.IN_PROGRESS;
    const saveData = {
        user,
        result,
        moves: moves?.length ? moves : []
    };

    const game = await Game.create(saveData);
    if (!game) throw AppError(500, 'Create Error', 'Something Went Wrong while creating new Game');

    await updateLastGame(game.user, game._id);

    return game;
};

export async function getGame(gameId) {
    const game = await Game.findById(gameId);
    if (!game) throw AppError(400, 'Not Found', 'Game record not found');

    return game;
};

export async function allGamesOfUser(userId) {
    const list = await Game.find({ user: userId });
    if (!list.length) return [];

    return list;
};

export async function updateGame({ result, moves, gameId }) {
    let game = await getGame(gameId);

    if (result) {
        if (result === GAME_RESULT.WON) updateGameWon(game.user);
        if (result === GAME_RESULT.LOOSE) updateGameLost(game.user);
        game.result = result
    };
    if (moves) game.moves.push(...moves);

    const saved = await game.save();
    if (!saved) throw AppError(500, 'Update Error', 'Something Went Wrong while updating game');

    return saved; 
};