function ApiRoutes(app) {

    app.use('/api/users', require('./api/users'));
    app.use('/api/games', require('./api/games'));

    app.route('/:url(api|auth|components|app|bower_components|assets)/*')
        .get((req, res) => {
            res.status(404).send('Route Not Found');
        });

    app.route('/*').get((req, res) => {
        res.status(404).send('Route Not Found');
    });
};

export default ApiRoutes;