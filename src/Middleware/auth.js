import { getUser } from "../api/users/service";
import { decodeToken } from "../utils/token.service";

export async function protect(req, res, next) {
    try {
        // Express headers are auto converted to lowercase
        let token = req.headers["x-access-token"] || req.headers["authorization"];
        if (token?.startsWith("Bearer ") || token?.startsWith("bearer ")) {
            // Remove Bearer from string
            token = token.split(" ")[1];
        };

        if (!token) {
            let error = new Error('You are not logged in! Please login in to continue');
            error.statusCode = 401;
            throw error;
        };

        const decode = decodeToken(token);

        const user = await getUser(decode.id);

        req.user = user;

        next();
    } catch (error) {
        next(error);
    }
};

export async function restrictTo(...roles) {
    return function(req, res, next) {
        if (!roles.includes(req.user.role)) {
            let error = new Error('Forbidden');
            error.statusCode = 403;
            next(error);
        };

        next();
    }
};