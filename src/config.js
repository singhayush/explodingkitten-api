import { config } from 'dotenv';
config();

export const PORT = process.env.PORT;
export const DATABASE = process.env.DATABASE.replace('<PASSWORD>', process.env.DATABASE_PASSWORD);
export const SECRET = process.env.SECRET;
export const JWT_EXPIRES_IN = process.env.JWT_EXPIRES_IN;
export const REDIS_SERVER = process.env.REDIS_SERVER || '127.0.0.1';
export const REDIS_PORT = process.env.REDIS_PORT || '6379';
export const REDIS_PASSWORD = process.env.REDIS_PASSWORD || null;