import { createClient } from "redis";
import { REDIS_SERVER, REDIS_PORT, REDIS_PASSWORD } from "../config";
import { promisify } from "util";
import { getLeaderBoardList } from "../api/users/service";
import { LeaderboardEvent } from "../api/users/events";


const client = createClient({ port: REDIS_PORT, host: REDIS_SERVER });
if (REDIS_PASSWORD) client.auth(REDIS_PASSWORD);

client.on("error", (err) => {
    console.log(err);
});

client.on('connect', () => {
    console.log('Successful Redis Connection');
});

const getAsync = promisify(client.get).bind(client);
const setAsync = promisify(client.set).bind(client);
const LEADERBOARD_KEY = 'Leaderboard';

export async function setLeaderboard(value) {
    try {
        value = JSON.stringify(value);
        const set = await setAsync(LEADERBOARD_KEY, value);
        if (!set) throw new Error('Redis Data Addition Failed');
    } catch (error) {
        console.log(error);
    }
};

export async function getLeaderboard() {
    try {
        let list = await getAsync(LEADERBOARD_KEY);
        if (list) {
            return JSON.parse(list);
        } else {
            list = await getLeaderBoardList();
            setLeaderboard(list);
            return list;
        };
    } catch (error) {
        console.log(error);
    }
};

export async function updateLeaderboard() {
    try {
        const list = await getLeaderBoardList();
        setLeaderboard(list);
    } catch (error) {
        console.log(error);
    }
};

async function emitLeaderboardSocket(io) {
    const data = await getLeaderboard();
    io.emit('Leaderboard', data);
};

export async function registerRedisSocket(io) {
    LeaderboardEvent.on('NewLeaderBoard', emitLeaderboardSocket(io));
};