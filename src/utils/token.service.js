import jwt from 'jsonwebtoken';
import { JWT_EXPIRES_IN, SECRET } from '../config';

export function generateToken(id) {
    return jwt.sign({id}, SECRET, {expiresIn: JWT_EXPIRES_IN});
};

export function decodeToken(token) {
    return jwt.verify(token, SECRET);
};