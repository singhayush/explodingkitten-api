import { registerGameSocket } from "../api/games/events";
import { registerRedisSocket } from "./redis.service";

function onSocketDisconnectListener(socket) {
    socket.on('disconnect', () => {
        console.log('Disconnected');
    });
};

function registerSocket(socket, io) {
    registerGameSocket(socket);
    registerRedisSocket(io);
};

export function onSocketConnectListener(socket, io) {
    return function () {
        console.log('Connection Made');
        onSocketDisconnectListener(socket);
        registerSocket(socket, io);
    };
};