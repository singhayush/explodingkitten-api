export function AppError (statusCode, status, message) {
    let error = new Error(message);
    error.statusCode = statusCode || 500;
    error.status = status || 'error';

    return error;
}