import { connect, connection } from 'mongoose';
import { DATABASE, PORT } from './config';
import socket from "socket.io";
import app from './app';
import { onSocketConnectListener } from './utils/socket.service';

process.on('uncaughtException', err => {
    console.log('UNCAUGHT EXCEPTION!!! shutting down...');
    console.log(err.name, err.message);
    process.exit(1);
});

// Connect the database
connect(DATABASE, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
}).then(() => {
    console.log('DB Connected Successfully!');
});

connection.on('error', function (err) {
    console.error(`MongoDB connection error: ${err}`);
    process.exit(-1);
});

// Start the Server
const server = app.listen(PORT, () => {
    console.log(`Application is running on port http://localhost:${PORT}`);
});

// Initiate Socket Connection
const io = socket(server);
io.on('connection', onSocketConnectListener(socket, io));

process.on('uncaughtException', err => {
    console.log('UNCAUGHT EXCEPTION!!! shutting down...');
    console.log(err.name, err.message);
    server.close(() => {
        process.exit(1);
    });
});